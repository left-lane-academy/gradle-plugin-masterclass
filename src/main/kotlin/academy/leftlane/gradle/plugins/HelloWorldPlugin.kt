package academy.leftlane.gradle.plugins

import academy.leftlane.gradle.tasks.MessageTask
import org.gradle.api.Plugin
import org.gradle.api.Project

class HelloWorldPlugin : Plugin<Project> {

    override fun apply(project: Project) {
        project.tasks.create("writeMessage", MessageTask::class.java)
    }
}
