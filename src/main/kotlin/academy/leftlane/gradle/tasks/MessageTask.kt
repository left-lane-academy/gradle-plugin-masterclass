package academy.leftlane.gradle.tasks

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

open class MessageTask : DefaultTask() {

    @TaskAction
    fun writeMessage() {
        println("Hello World, from the second plugin!")
    }
}
