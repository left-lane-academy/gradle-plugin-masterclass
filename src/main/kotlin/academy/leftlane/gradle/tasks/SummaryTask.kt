package academy.leftlane.gradle.tasks

import academy.leftlane.gradle.extensions.SummaryExtension
import org.dom4j.io.SAXReader
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction
import java.io.File
import javax.inject.Inject

open class SummaryTask @Inject constructor(
    private val extension: SummaryExtension
) : DefaultTask() {

    private val coverageTypeCache: HashMap<String, Float> = hashMapOf()

    @TaskAction
    fun extractTestReport() {
        if (!extension.enabled) return

        for (module in extension.modulesContainerInstance.namesAndReports) {
            val reportPath = module.reportPath
            val document = SAXReader().read(File(reportPath))
            val coverageTypes = document.rootElement.elements("counter")

            for (coverageType in coverageTypes) {
                val coverageTypeName = coverageType.attribute("type").value.toLowerCase().capitalize()

                val covered = coverageType.attribute("covered").value.toFloat()
                val missed = coverageType.attribute("missed").value.toFloat()
                val percentage = (covered / (covered + missed)) * 100f

                addToCache(coverageTypeName, percentage)
            }
        }

        val totalModules = extension.modulesContainerInstance.namesAndReports.count().toFloat()

        for (key in coverageTypeCache.keys) {
            coverageTypeCache[key]?.let {
                val percentage = it / totalModules
                println("Summary ${key.capitalize()} - ${"%.2f".format(percentage)}%")
            }
        }
    }

    private fun addToCache(coverageTypeName: String, percentage: Float) {
        val name = coverageTypeName.toLowerCase()

        if (!coverageTypeCache.containsKey(name)) {
            coverageTypeCache[name] = 0f
        }

        coverageTypeCache[name]?.let {
            coverageTypeCache[name] = it + percentage
        }
    }
}
