package academy.leftlane.gradle

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

open class MessageTask : DefaultTask() {

    @TaskAction
    fun writeMessage() {
        println("Hello Universe, from the third plugin!")
    }
}
